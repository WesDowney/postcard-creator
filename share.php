<!doctype html>

<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

	<title>Postcard Creator: Share</title>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
	<link rel="stylesheet" href="css/main.css">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

	<div class="container">
  		<div class="row">
  			<div id="postcard">
	  			<div id="finished-postcard">
					<img src="postcards/<?php echo $_GET["postcard"] ?>" alt="Your Postcard!" width="640" height="480">
				</div>
			</div>
  		</div>

		<div class="row">
			<div id="share">
				<h2>Send it to a friend!</h2>
				<form action="sent.php" method="post" id="contactme">
					<div class="form-group">
						<input type="text" class="form-control" name="name" id="name" placeholder="Your First Name" autocomplete="off">
					</div>
					<div class="form-group">
						<input type="text" class="form-control" name="email" id="email" placeholder="Your Friend's Email" autocomplete="off">
					</div>
					<div class="form-group">
						<textarea class="form-control" rows="3" name="message" id="message" placeholder="Add a message ..."></textarea>
					</div>

					<input type="hidden" name="image" value="<?php echo $_GET["postcard"] ?>" />

					<button type="submit" class="btn btn-primary btn-lg btn-block wide-button" id="share">Share Postcard</button>
				</form>
			</div>
		</div>

  	</div>


<!-- Include all compiled plugins (below), or include individual files as needed -->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
<script src="js/share.js"></script>

</body>
</html>