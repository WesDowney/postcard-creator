<!--

Mobile: 
	<form method="post" enctype="multipart/form-data">
		<input type="file" name="image" accept="image/*" capture>
		<input type="submit" value="Upload">
	</form>

Flash for Safari

Might have to upgrade JQuery

-->

<!doctype html>

<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

	<title>Postcard Creator</title>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
	<link rel="stylesheet" href="css/literallycanvas.css">
	<link rel="stylesheet" href="css/joyride-2.1.css" type="text/css">
	<link rel="stylesheet" href="css/main.css">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

	<div class="container">
		<div class="row" id="headline">
			<h1>Postcard Creator</h1>
		</div>
  		<div class="row">
  			
  			<div id="postcard">
	  			<div id="webcam">
					<video id="video" width="640" height="480" autoplay></video>
				</div>
				<div class="my-drawing" id="picture">
					<canvas id="snapshot" width="640" height="480"></canvas>
				</div>
			</div>
			
			<button type="button" class="btn btn-primary btn-lg btn-block wide-button" id="snap">Snap Photo</button>
  		</div>
  	</div>

	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script data-require="jquery@*" data-semver="2.0.3" src="//code.jquery.com/jquery-2.0.3.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
	<!-- React.js is a dependacy of Literallycanvas.js --> 
	<script src="//cdnjs.cloudflare.com/ajax/libs/react/0.14.7/react-with-addons.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/react/0.14.7/react-dom.js"></script>
	<script src="js/literallycanvas.js"></script>
	<script src="js/jquery.joyride-2.1.js"></script>
	<script src="js/main.js"></script>

<ol id="joyRideTipContent">
	<li data-id="postcard" data-options="tipLocation:left;tipAnimation:fade">Nice Shot! Use the tools here to personalize it.</li>
	<li data-class="lc-picker" data-options="tipLocation:bottom;tipAnimation:fade">If you make a mistake, you can clear the changes here.</li>
	<li data-id="snap">When you're done making your postcard awesome, click here to share it with a friend.</li>
</ol>

</body>
</html>