// Grab elements, create settings, etc.
var video = document.getElementById('video');

// Get access to the camera!
if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
    // Not adding `{ audio: true }` since we only want video now
    navigator.mediaDevices.getUserMedia({ video: true }).then(function(stream) {
        video.src = window.URL.createObjectURL(stream);
        video.play();
    });
} else {
    alert("No web cam detected. Please use a desktop Chrome or FireFox browser to continue.");
}

/* Legacy code below: getUserMedia 
else if(navigator.getUserMedia) { // Standard
    navigator.getUserMedia({ video: true }, function(stream) {
        video.src = stream;
        video.play();
    }, errBack);
} else if(navigator.webkitGetUserMedia) { // WebKit-prefixed
    navigator.webkitGetUserMedia({ video: true }, function(stream){
        video.src = window.webkitURL.createObjectURL(stream);
        video.play();
    }, errBack);
} else if(navigator.mozGetUserMedia) { // Mozilla-prefixed
    navigator.mozGetUserMedia({ video: true }, function(stream){
        video.src = window.URL.createObjectURL(stream);
        video.play();
    }, errBack);
}
*/

// Elements for taking the snapshot
var snapshot = document.getElementById('snapshot');
var context = snapshot.getContext('2d');
var video = document.getElementById('video');
var snap = document.getElementById("snap");

// Trigger photo take
snap.addEventListener("click", function() {
    if (snap.innerText == "Snap Photo"){
        // Draws the snapshot from the web cam
        context.drawImage(video, 0, 0, 640, 480);

        // Save the snapshot to a data file
        var backgroundImage = new Image();
        backgroundImage.src = snapshot.toDataURL("image/png");

        // Expand the container to hold the toolbars
        document.getElementById("postcard").style.width = "720px";
        document.getElementById("postcard").style.height = "531px";
        snap.style.width = "720px";

        snap.innerText = "Share";
        snap.classList.add("btn-success");

    	// Start LiterallyCanvas
        LC.init(
            document.getElementsByClassName('my-drawing')[0],
            {
                imageURLPrefix: 'img',
                backgroundShapes: [
                    LC.createShape(
                      'Image', {x: 0, y: 0, image: backgroundImage})
                ]
            }
        );

        $("#joyRideTipContent").joyride({
            autoStart : true
        });

    } else { // The button clicked was "Share"
        
        var email = "wesdowney@gmail.com";
        
        // Flatten the canvas background and effects
        context.drawImage(document.getElementsByTagName('canvas')[0], 0, 0, 640, 480);
        context.drawImage(document.getElementsByTagName('canvas')[1], 0, 0, 640, 480);

        var savedImage = new Image();
        savedImage.src = snapshot.toDataURL("image/png");

        $.ajax({ 
            url: 'save-postcard.php',
            data: 
                {
                    image: savedImage.src
                },
                type: 'post',
                success: function(output) {
                    // Redirect to the share page
                    window.location.href = "share.php?postcard=" + output;
                }
        });
    }
});