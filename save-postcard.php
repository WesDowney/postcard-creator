<?php

error_reporting(E_ALL);

// Convert base64 to PNG
$data = $_POST['image'];
$data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data));

// Set a random name for the image
$imageName = substr(md5(rand()), 0, 30) . ".png";
$imagePath = "postcards/" . $imageName;

file_put_contents($imagePath, $data);

// Passes the image name back to AJAX
echo $imageName;

?>