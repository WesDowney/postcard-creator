# README #

### Which application you choose to develop and why 

* I chose to develop the Postcard Creator because I would learn the most from building it. I would have been more comfortable building the Twitter API or login session project since I've done things like those before but a challenge helps me grow. With this project, I learned about HTML5 Canvas, accessing web cams and a lot more. 

### How to use/test the provided application 

* Transfer all the files onto a LAMP server
* CHMOD the "postcards" folder to "777". This is needed to upload the user post cards.
* A valid SSL certicate is required if on a live web server (not localhost). One can be obtained for free from https://letsencrypt.org
* Access the application at "index.php" from a desktop with Chrome, Firefox or one of the other browsers that support the getUserMedia API. List here: http://caniuse.com/#feat=stream
* You can also see the app live on my site: https://wesdowney.com/postcard-creator/

### What Operating System (+ service pack) and libraries are required 

* This was built on Ubuntu 16.04 but it should work on most Linux servers set up with the LAMP stack.
* Sendmail package is commonly already included on LAMP stack but is required due to PHP's mail() API.

###  Any design decisions or behavioral clarifications that illustrate how your program functions and why 
* Currently the application works with desktop web cams but I'd like to build it to also accept image uploads or taking pictures from mobile if a web cam isn't available. The rest of the code and external libararies should be mobile optimized.

###  If you use any external libraries or code-snippets, you must provide the following information for each ( credit must be given to others)  

	○ Name: Bootstrap
	○ Version: v4.0.0 Alpha 6
	○ Purpose: Styling
	○ License: MIT
	○ Website(s): http://www.getbootstrap.com

	○ Name: JQuery
	○ Version: v2.0.3
	○ Purpose: Bootstrap's JavaScript plugins, Literally Canvas and jQuery Joyride
	○ License: MIT
	○ Website(s): http://www.jQuery.com

	○ Name: David Walsh - Browser Camera Code Snippet
	○ Version: n/a
	○ Purpose: To initialize the web cam and provide a base to build off
	○ License: MIT
	○ Website(s): https://davidwalsh.name/browser-camera

	○ Name: Literally Canvas
	○ Version: 0.4.14	
	○ Purpose: Paint like tools for modifying the canvas.
	○ License: BSD
	○ Website(s): http://literallycanvas.com

	○ Name: PHPMailer
	○ Version: v5.2.22
	○ Purpose: Easier email handling with attachments
	○ License: LGPL-2.1
	○ Website(s): https://github.com/PHPMailer/PHPMailer

	○ Name: jQuery Joyride
	○ Version: v2.1
	○ Purpose: Provide onscreen instructions when editing snapshots
	○ License: MIT
	○ Website(s): http://zurb.com/playground/jquery-joyride-feature-tour-plugin

###  What tools/libraries (and versions) are necessary to use and test your application
* A browser that supports getUserMedia is required. List here: http://caniuse.com/#feat=stream
* SSL certificate - Browser vendors require this for access to the getUserMedia API. I used LetsEncrypt since it is free and is becoming the leader for SSL. I set it to autorenew. https://letsencrypt.org/getting-started/