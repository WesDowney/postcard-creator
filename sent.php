

<!doctype html>

<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

	<title>Postcard Creator: Postcard Sent!</title>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
	<link rel="stylesheet" href="css/main.css">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

	<div class="container">
  		<div class="row">
  			<div id="sent">
  			
				<?php 

					require_once('PHPMailer/class.phpmailer.php');

					$subject = $_POST["name"] . " has sent you a post card!";
					$body = $_POST["name"] . "'s message: \"" . $_POST["message"] . "\"<br /><br />";
					$body .= "Why don't you send " . $_POST["name"] . " one back? Try it free at http://www.wesdowney.com/postcard-creator";

					// Send email with PHP Mailer
					$email = new PHPMailer();
					$email->IsHTML(true);
					$email->From      = 'postcard@wesdowney.com';
					$email->FromName  = 'PostCard';
					$email->Subject   = $subject;
					$email->Body      = $body;
					$email->AddAddress( $_POST['email'] );
					$file_to_attach = "postcards/" . $_POST['image']; // path to image
					$email->AddAttachment( $file_to_attach , $_POST['image'] );

					if ($email->Send()) {
						echo "<h2><em>Your postcard is on it's way!</em></h2>";
					} else {
						echo "<h2><em>Sorry! There was an error and the postcard failed to send.</em></h2>";
					}

				?>

				<a href="index.php" class="btn btn-primary" id="make-another">Make Another Postcard</a>
			</div>
  		</div>
  	</div>


<!-- Include all compiled plugins (below), or include individual files as needed -->
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>

</body>
</html>